﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class MeshTiles : MonoBehaviour
{

    public Material material;
    public int width = 16;
    public int height = 9;
    public int dimension = 4;
    public float scaleHeight = 0.2f;
    public bool generateEnvironment = true;

    private int chunkWidth, chunkHeight, numberOfChunks;

    private Mesh mesh;

    private List<SubTile> meshList;
    private MeshEntry[,] meshMatrix;
    private List<EnvironmentGeneration> enviromentList;

    [SerializeField]
    public Plant[] plants;
    public bool collisionOn = true;


    void Start()
    {
        meshList = new List<SubTile>();
        enviromentList = new List<EnvironmentGeneration>();
      
        CreateMesh(width, height, dimension);
      
       
    }


    private void Update() {
        if (Input.GetKeyUp(KeyCode.G) || OVRInput.Get(OVRInput.Button.One)) {
            foreach (EnvironmentGeneration enviromentGeneration in enviromentList) {
                enviromentGeneration.GenerateTerrain(plants);
            }
        }
    }

    void CreateMesh(int xAmount, int zAmount, int dimension)
    {
        chunkHeight = zAmount;
        chunkWidth = xAmount;
        numberOfChunks = chunkWidth * chunkHeight;
        meshMatrix = new MeshEntry[xAmount, zAmount];

        for (int z = 0; z < zAmount; z++) {
            for (int x = 0; x < xAmount; x++)
            {
            
                GameObject g = CreateTile(x, z, dimension);
                if (generateEnvironment == true) {
                    g.AddComponent<EnvironmentGeneration>();
                    g.GetComponent<EnvironmentGeneration>().Init(plants[0].GetModel(), g);

                   
                }
                
                meshList.Add(g.GetComponent<SubTile>());
               

                enviromentList.Add(g.GetComponent<EnvironmentGeneration>());
                g.AddComponent<MeshCollider>();

                g.transform.parent = this.transform;
            }
        }
    }

    GameObject CreateTile(int currentX, int currentY, int dimension)
    {
        GameObject gameObject = new GameObject($"Tile({dimension}) : {currentX}-{currentY}");
        Vector2 position = new Vector2(dimension * currentX - (width * dimension) / 2f,  dimension * currentY - (height * dimension) / 2f);
        gameObject.transform.position = new Vector3( position.x,0, position.y);

       

        MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
        meshRenderer.material = material;

        mesh = new Mesh();
        MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        SubTile subTile = gameObject.AddComponent<SubTile>();
        subTile.Init(currentX, currentY, ref mesh, dimension, dimension, this.height, this.width);


        meshMatrix[currentX, currentY] = new MeshEntry(gameObject.GetComponent<SubTile>(), position, dimension);

        return gameObject;
    }

    public void ReceiveChunkUpdate(int[] chunk, int size, int chunkIndex) {

        meshList[chunkIndex].ReceiveUpdate(chunk, size, scaleHeight);
    }

    public void UpdateTexture(ref Texture2D texture) {
        material.mainTexture = texture;
    }

    public Vector3 GetPositionOfVertex(int chunkindex, int indexX, int indexY) {
        return meshList[chunkindex].GetHeightFromPosition(indexX, indexY);
    
    }

    public int getChunkHeight() {
        return chunkHeight;
    }

    public int getChunkWidth() {
        return chunkWidth;
    }

    public int getNumberOfChunks() {
        return numberOfChunks;
    }

    public Vector3 GetVertexFromGlobalPosition(Vector2 globalPosition) {
        SubTile currentMesh = meshList[0];
        MeshEntry finalentry = meshMatrix[0,0];
        foreach (MeshEntry entry in meshMatrix) {

            if (entry.IsPositionInChunk(globalPosition)) {
                currentMesh = entry.GetSubTile();
                globalPosition = globalPosition - entry.GetPosition();
                finalentry = entry;
                break;
            }
        }
       //TO-DO Do Real Position with y
       return currentMesh.GetPositionInChunk(globalPosition) + new Vector3(finalentry.GetPosition().x, 0, finalentry.GetPosition().y);

    }

    

  

}

public class MeshEntry {
    private SubTile subtile;
    private float minX, minZ, maxX, maxZ;
    private int index;
    private Vector2 position;

    public MeshEntry(SubTile subtile, Vector2 position, int dimension) {
        minX = position.x;
        minZ = position.y;

        maxX = minX + dimension;
        maxZ = minZ + dimension;
        this.position = new Vector2(minX, minZ);
      
        this.subtile = subtile;
    }

    public int GetIndex() {
        return this.index;
    }

    public SubTile GetSubTile() {
        return subtile;
    }

    public bool IsPositionInChunk(Vector2 position) {
        return position.x >= minX && position.x <= maxX && position.y >= minZ && position.y <= maxZ;
    }

    public Vector2 GetPosition() {
        return this.position;
    }
  
}
