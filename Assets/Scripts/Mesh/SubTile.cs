﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubTile : MonoBehaviour
{
    public int size;
    MeshRenderer renderer;
    MeshFilter filter;

    private Vector3[] vertices;
    private Vector2[] uvs;
    private int[] triangles;
    private Mesh mesh;
    private float scaleHeight;
    int chunkWidth, chunkHeight;

    int[] heightData;
    bool heightDataHasChanged = false;
    //int nextUpdateInFrames = 100000000;
    MeshCollider meshCollider;
    void Update() {
        if (heightDataHasChanged) {
            for (int index = 0; index < heightData.Length; index++) {

                //vertices[index].y = Random.Range(0, 10);  //data[index] * scaleHeight;
                //Vector3 v = new Vector3();
                Vector3 vec = vertices[index];
                vec.y = (heightData[index] - 500) * scaleHeight;
                vertices[index] = vec;

            }
           
            

            mesh.vertices = vertices;
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            


            this.GetComponent<EnvironmentGeneration>().UpdatePlantPosition();
            heightDataHasChanged = false;
        }
        
    }

    private void Start() {
        meshCollider = gameObject.GetComponent<MeshCollider>();
    }

    public void Init(int xStart, int yStart, ref Mesh mesh, int chunkWidth, int chunkHeight, int meshWidth, int meshHeight){

        this.mesh = mesh;
        BuildMesh(xStart, yStart, chunkWidth + 1, chunkHeight + 1, meshWidth, meshHeight);
        renderer = GetComponent<MeshRenderer>();
        filter = GetComponent<MeshFilter>();
        this.chunkHeight = chunkHeight;
        this.chunkWidth = chunkWidth;
    }

    void BuildMesh(int xStart, int yStart, int chunkWidth, int chunkHeight, int meshDimensionX, int meshDimensionZ) {
        vertices = new Vector3[chunkWidth * chunkHeight];
        triangles = new int[chunkWidth * chunkHeight * 6];
        uvs = new Vector2[chunkWidth * chunkHeight];

        int numberOfVerteciesX = meshDimensionZ * chunkWidth;
        int numberOfVerteciesY = meshDimensionX * chunkHeight;
        // initialize the mesh surface
        int index = 0;
       
          for (int y = 0; y < chunkHeight; y++) {
               for (int x = 0; x < chunkWidth; x++) {


                //int y = heightData[x * zDimension + x];
                float height = -20;
                vertices[index] = new Vector3(x, height, y);
                uvs[index] = (new Vector2(((float)(xStart * chunkWidth + x) / numberOfVerteciesX), (float)(yStart * chunkHeight + y) / numberOfVerteciesY));
                index++;
            }
        }

        // creates quad on available space
        int currentTriangle = 0;
        for (int z = 0; z < chunkHeight - 1; z++) {
            for (int x = 0; x < chunkWidth - 1; x++) {
           
                triangles[currentTriangle + 0] = (x + chunkWidth * z);
                triangles[currentTriangle + 1] = (x + chunkWidth * (z + 1));
                triangles[currentTriangle + 2] = (x + 1 + (chunkWidth) * (z));

                triangles[currentTriangle + 3] = (x + 1 + chunkWidth * z);
                triangles[currentTriangle + 4] = (x + chunkWidth * (z + 1));
                triangles[currentTriangle + 5] = (x + 1 + chunkWidth * (z + 1));

                currentTriangle += 6;
                

            }
        }

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
    }

    /// <summary>
    /// Receive Update
    /// </summary>
    /// <param name="data">heightdata</param>
    public void ReceiveUpdate(int[] data, int chunkSize, float scaleHeight) {

        heightData = data;
        this.scaleHeight = scaleHeight;
        heightDataHasChanged = true;
    }

    public Vector3 GetHeightFromPosition(int x, int z) {
        return this.vertices[x + z * chunkWidth];
    }
    public int getWidth() {
        return this.chunkWidth;
    }

    public int getHeight() {
        return this.chunkHeight;

    }

    public void UpdateMeshCollider() {
        meshCollider.sharedMesh = mesh;

    }

    public Vector3 GetPositionInChunk(Vector2 positionInChunk) {
       
        Vector3 clostestPoint = vertices[0];
        float currentDistance = float.MaxValue;
        float bestDistance = float.MaxValue;
        foreach (Vector3 vertex in vertices) {

            currentDistance =( positionInChunk - new Vector2(vertex.x, vertex.z)).magnitude;
   
            if (currentDistance < bestDistance) {
                
                clostestPoint = vertex;
                bestDistance = currentDistance;
            }
        }

        return clostestPoint;
    }
}
