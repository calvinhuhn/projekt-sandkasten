﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCorners : MonoBehaviour
{

    Vector3[] corners;
    Mesh quadMesh;
    // Start is called before the first frame update
    void Start()
    {
        quadMesh = GetComponent<MeshFilter>().mesh;
        corners = quadMesh.vertices;
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKey(KeyCode.Alpha1)) {
            if (Input.GetKey(KeyCode.LeftArrow)) {
                ChangeCorner(0, Vector3.left);
            }
            if (Input.GetKey(KeyCode.RightArrow)) {
                ChangeCorner(0, Vector3.right);
            }
            if (Input.GetKey(KeyCode.DownArrow)) {
                ChangeCorner(0, Vector3.down);
            }
            if (Input.GetKey(KeyCode.UpArrow)) {
                ChangeCorner(0, Vector3.up);
            }
        }

        if (Input.GetKey(KeyCode.Alpha3)) {
            if (Input.GetKey(KeyCode.LeftArrow)) {
                ChangeCorner(1, Vector3.left);
            }
            if (Input.GetKey(KeyCode.RightArrow)) {
                ChangeCorner(1, Vector3.right);
            }
            if (Input.GetKey(KeyCode.DownArrow)) {
                ChangeCorner(1, Vector3.down);
            }
            if (Input.GetKey(KeyCode.UpArrow)) {
                ChangeCorner(1, Vector3.up);
            }
        }

        if (Input.GetKey(KeyCode.Alpha2)) {
            if (Input.GetKey(KeyCode.LeftArrow)) {
                ChangeCorner(2, Vector3.left);
            }
            if (Input.GetKey(KeyCode.RightArrow)) {
                ChangeCorner(2, Vector3.right);
            }
            if (Input.GetKey(KeyCode.DownArrow)) {
                ChangeCorner(2, Vector3.down);
            }
            if (Input.GetKey(KeyCode.UpArrow)) {
                ChangeCorner(2, Vector3.up);
            }
        }

        if (Input.GetKey(KeyCode.Alpha4)) {
            if (Input.GetKey(KeyCode.LeftArrow)) {
                ChangeCorner(3, Vector3.left);
            }
            if (Input.GetKey(KeyCode.RightArrow)) {
                ChangeCorner(3, Vector3.right);
            }
            if (Input.GetKey(KeyCode.DownArrow)) {
                ChangeCorner(3, Vector3.down);
            }
            if (Input.GetKey(KeyCode.UpArrow)) {
                ChangeCorner(3, Vector3.up);
            }
        }
    }

    void ChangeCorner(int cornerIndex, Vector3 direction) {
        float scale = 0.01f;
        corners[cornerIndex] += direction * scale;
        quadMesh.vertices = corners;
    }
}
