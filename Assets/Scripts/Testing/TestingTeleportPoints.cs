﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingTeleportPoints : MonoBehaviour
{

    public GameObject testObject;
    private MeshTiles mesh;
    // Start is called before the first frame update
    void Start()
    {
        mesh = testObject.GetComponent<MeshTiles>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.I)) {
            Debug.Log(mesh.GetVertexFromGlobalPosition(new Vector2(-132, 45f)));
        }
        
    }
}
