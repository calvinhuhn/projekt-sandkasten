﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRCharacterMovement : MonoBehaviour
{
    /* ========= public ============= */
    public int maxIterations = 10;
    public int curveDetail = 10;
    public float stepSize = 0.5f;
    public float controlPointHeight = 5;
    public float offset = 0.3f;
    public GameObject mesh;
    public GameObject Player;

    public Color startColor;
    public Color endColor;
    public float startWidth;
    public float endWidth;
    public Material m;
   
    /* ========== Private Variables ============ */
    private Vector3 direction;

    private Vector3 startpoint;
    private Vector3 endpoint;
    private Vector3 controlpoint;

    private Vector3 teleportationPoint;
    public Vector3 personHeight;

    private bool intersectionFound = false;
    
    private MeshTiles meshController;
    private GameObject myLine;
    private GameObject rightController;
    public GameObject camera;
    public int teleportCooldown = 0;
    

    int numberOfPoints = 0;
    // Start is called before the first frame update
    void Start()
    {
        direction = transform.forward;
        meshController = mesh.GetComponent<MeshTiles>();

       
       

    }

    // Update is called once per frame
    void Update()
    {
        if (rightController == null) {
            rightController = GameObject.Find("controller_right");
        }
        if (teleportCooldown > 0) {
            teleportCooldown--;
        }
        //Destroys Object each frame

        if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch) > 0.5f) {
            Debug.Log("ping");
            direction = rightController.transform.forward;

            CalculateIntersection();

            if (intersectionFound) {
                Debug.Log("Intersect Line");
                startpoint = rightController.transform.position;

                CalculateControllPoint(startpoint, endpoint);
                DrawLine(startpoint, endpoint, Color.blue, curveDetail);
            }

            if (teleportCooldown <= 0 && OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch) > 0.5f) {
                Debug.Log("Transformed to another Location.");
                transform.position = teleportationPoint + personHeight;
                camera.transform.position = teleportationPoint + personHeight;
                teleportCooldown = 10;
                //Player.transform.position = teleportationPoint + personHeight;
            }
        }
        
    }

    /// <summary>
    /// Calculates the Intersection with the Raydirection and the Mesh
    /// </summary>
    private void CalculateIntersection() {
        Vector3 temp;
        float step = 0.01f;
        int iterations = 0;
        intersectionFound = true;
        do {
            step += stepSize;
            temp = rightController.transform.position + rightController.transform.forward * step;
            iterations++;
            if (iterations > maxIterations) {
                intersectionFound = false;
                break;
            }
        } while (!PointIsUnderGroundLevel(temp));

        if (intersectionFound == true) {
            endpoint = temp;

        }
    }

    /// <summary>
    /// Calculates the Controllpoint for Bezier Curve
    /// </summary>
    /// <param name="startpoint"></param>
    /// <param name="endpoint"></param>
    private void CalculateControllPoint(Vector3 startpoint, Vector3 endpoint) {
        controlpoint = startpoint + (endpoint - startpoint) / 2;
        controlpoint.y = controlpoint.y + controlPointHeight;
    }

    /// <summary>
    /// Samples the Bezier Curve from Startpoint to Endpoint with a given Controllpoint
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="control"></param>
    /// <param name="t"></param>
    /// <returns></returns>
    Vector3 SampleCurve(Vector3 start, Vector3 end, Vector3 control, float t) {
        // Interpolate along line S0: control - start;
        Vector3 Q0 = Vector3.Lerp(startpoint, controlpoint, t);
        // Interpolate along line S1: S1 = end - control;
        Vector3 Q1 = Vector3.Lerp(controlpoint, endpoint, t);
        // Interpolate along line S2: Q1 - Q0
        Vector3 Q2 = Vector3.Lerp(Q0, Q1, t);

        return Q2; // Q2 is a point on the curve at time t
    }


    /// <summary>
    /// Draw Throw Line
    /// </summary>
    /// <param name="start">Start Point from Teleportposition, mostly Player Position or Controller Position</param>
    /// <param name="end">End point from Teleportation, mostly the Intersection with ground</param>
    /// <param name="color">Color of the line</param>
    /// <param name="detail">Detailness of lines</param>
    /// <param name="duration">Lifespan of Line</param>
    void DrawLine(Vector3 start, Vector3 end, Color color, int detail, float duration = 0.1f) {
        
        myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = m;
        lr.positionCount = detail;
        lr.startColor = startColor;
        lr.endColor = endColor;
        lr.startWidth = startWidth;
        lr.endWidth = endWidth;

        for (int i = 0; i < detail; i++) {
            lr.SetPosition(i, SampleCurve(startpoint, endpoint, controlpoint, i / (float)detail));
        }

        //TODO Dont destroy Line. 
        //Change values instead
        GameObject.Destroy(myLine, 0.05f);


    }

    /// <summary>
    /// Checks if the Teleportation is under Ground Level and saves the teleportation Point
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    bool PointIsUnderGroundLevel(Vector3 position) {
        teleportationPoint = meshController.GetVertexFromGlobalPosition(new Vector3(position.x, position.z));
        teleportationPoint = new Vector3(position.x, teleportationPoint.y, position.z);
        return position.y < teleportationPoint.y;
    }


}
