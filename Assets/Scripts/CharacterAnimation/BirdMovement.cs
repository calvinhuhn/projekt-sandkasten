﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdMovement : MonoBehaviour
{
    public float speed = 0.5f;
    public float radius = 5f;
    public float seed = 2.1f;
    public float movementDetail = 10;
    public float offset = 1.3f;

    public float directionChangeInterval = 1;
    public float maxHeadingChange = 30;
    public Vector3 centerpoint = new Vector3(0, 0, 0);
    float toCenter;

    float heading;
    Vector3 targetRotation;
    Vector3 centerPoint;


    float x, z = 0;
    float xCoord, yCoord = 0;
    Vector3 temp;
    Vector3 rotation;

    void Start() {
        centerPoint = gameObject.transform.position;
    }

    void Awake() {
     

    }

    void Update() {
        x = x + speed;
        z = z + speed;
        if (x > Mathf.PI * 2) {
            x = x - Mathf.PI * 2;
            z = z - Mathf.PI * 2;
        }

        //temp = new Vector3(Mathf.Sin(x) * radius, 0, Mathf.Cos(x) * radius);
        temp = MovementFunction(x);
 
        temp = MovementFunction(x);
        gameObject.transform.position = (centerPoint + temp);
        rotation = temp - MovementFunction(x - speed);
        gameObject.transform.forward = rotation;



    }



    private Vector3 MovementFunction(float t) {
        //Create a Circle
        xCoord = Mathf.Sin(t) * movementDetail;
        yCoord = Mathf.Sin(t + offset) * movementDetail;

        //Over Perlinnoise map
        return new Vector3((seed + Mathf.PerlinNoise(xCoord, yCoord) - 0.5f) * radius,0, seed + (Mathf.PerlinNoise(yCoord, xCoord) - 0.5f)* radius);

    }
}
