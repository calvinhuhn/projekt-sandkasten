﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentGeneration : MonoBehaviour
{
    public GameObject meshTile;
 
   
    public int width, height;
    SubTile subTile;
    int totalVertecies;

    Dictionary<Vector2Int, PlantObject> gameObjects;
   
    Plant[] plants;



    public void Init(GameObject model, GameObject tile) {
        meshTile = tile;
        gameObjects = new Dictionary<Vector2Int, PlantObject>();
        subTile = meshTile.GetComponent<SubTile>();
        width = subTile.getWidth();
        height = subTile.getHeight();
        totalVertecies = width * height;
        this.plants = new Plant[0];
 
    }
    void Start()
    {
        gameObjects = new Dictionary<Vector2Int, PlantObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateTerrain(Plant[] plants) {
        DeletePlants();
        foreach (Plant plant in plants) {
            GeneratePlant(plant);
        }
        this.plants = plants;
    }

    public void DeletePlants() {
        foreach (KeyValuePair<Vector2Int, PlantObject> currentGameObject in gameObjects) {
            Destroy(currentGameObject.Value.getObjectReference());
        }
        gameObjects = new Dictionary<Vector2Int, PlantObject>();
    }

    public void GeneratePlant(Plant plant) {

        int randomPositionX = 0;
        int randomPositionY = 0;
        
        Vector3 position;
        for (int i = 0; i < plant.GetFrequency() * totalVertecies; i++) {

            randomPositionX = (int)Random.Range(0, width);
            randomPositionY = (int)Random.Range(0, height);
            position = subTile.GetHeightFromPosition(randomPositionX, randomPositionY);
            
            if (positionIsValid(position, plant)) {
                GameObject current = Instantiate(plant.GetModel(), gameObject.transform.position, Quaternion.identity);
                //TODO check for collisions
                
                try {
                  
                    gameObjects.Add(new Vector2Int(randomPositionX, randomPositionY), new PlantObject(plant, current));
                } catch {

                }
                current.transform.parent = gameObject.transform;
                current.transform.localPosition = position;
           
                /*
                Animation animation = current.GetComponent<Animation>();
                animation["Idle"].time = Random.Range(0.0f, animation["Idle"].length);
                */
                Vector3 orientation = plant.GetOrientation();
                current.transform.localRotation = Quaternion.Euler(orientation.x + Random.Range(0, 10f), Random.Range(0, 180), orientation.z + Random.Range(0, 10f));
                float scale = Random.Range(0.85f, 1.15f);
                current.transform.localScale = (new Vector3(plant.GetScale() * scale, plant.GetScale() * scale, plant.GetScale() * scale));
            }
        }
    }

    public void UpdatePlantPosition() {
        //Check if PlantPosition is still Valid
        List<Vector2Int> toBeRemoved = new List<Vector2Int>();
        foreach (KeyValuePair<Vector2Int, PlantObject> currentGameObject in gameObjects) {
            Vector3 position = subTile.GetHeightFromPosition(currentGameObject.Key.x, currentGameObject.Key.y);
            currentGameObject.Value.getObjectReference().transform.localPosition = position;
            if (!currentGameObject.Value.getValidator().HeightIsValid(position.y)) {
                Destroy(currentGameObject.Value.getObjectReference());
                toBeRemoved.Add(currentGameObject.Key);   
            } 
        }

        foreach (Vector2Int key in toBeRemoved) {
            gameObjects.Remove(key);
        }
    }

   

    private bool positionIsValid(Vector3 position, Plant plant) {
        if (position.y < plant.GetMinimalSurvivalHeight() || position.y > plant.GetMaximalSurvivalHeight()) {
            return false;
        }
        return true;
    }

   
    
}

public class PlantObject {
    private Plant objectRules;
    private GameObject objectReference;

    public PlantObject(Plant objectRule, GameObject objectReference) {
        this.objectRules = objectRule;
        this.objectReference = objectReference;
    }

    public Plant getValidator() {
        return objectRules;
    }

    public GameObject getObjectReference() {
        return objectReference;
    }
}