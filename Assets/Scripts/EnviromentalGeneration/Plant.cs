﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Plant
{
    public string name;
    public GameObject model3D;
    public float frequency;
    public int minimalSurvivalHeight, maximalSurvivalHeight;
    public Vector3 orientation;
    public float scale;


    public Plant(string name, GameObject model3D, float frequency, int minimalSurvivalHeight, int maximalSurvivalHeight, Vector3 orientation, float scale) {
        this.name = name;
        this.model3D = model3D;
        this.frequency = frequency;
        this.minimalSurvivalHeight = minimalSurvivalHeight;
        this.maximalSurvivalHeight = maximalSurvivalHeight;
        this.orientation = orientation;
        this.scale = scale;
    }

    public float GetScale() {
        return scale;
    }

    public bool HeightIsValid(float height) {
        return height > minimalSurvivalHeight && height < maximalSurvivalHeight;
    }

    public int GetMinimalSurvivalHeight() {
        return this.minimalSurvivalHeight;
    }

    public int GetMaximalSurvivalHeight() {
        return this.maximalSurvivalHeight;
    }

    public float GetFrequency() {
        return this.frequency;
    }

    public GameObject GetModel() {
        return this.model3D;

    }

    public string GetName() {
        return name;

    }

    public Vector3 GetOrientation() {
        return this.orientation;

    }

    public override string ToString() {
        return " Name: " + name + " Frequency: " + frequency + " min: " + minimalSurvivalHeight + " max: " + maximalSurvivalHeight;
    }
}
