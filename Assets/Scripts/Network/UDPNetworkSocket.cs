﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System;


public class UDPNetworkSocket : MonoBehaviour {
    public string ip = "169.254.107.186";
    Socket s;


    IPAddress broadcast;
    // Start is called before the first frame update
    void Start() {
        s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        broadcast = IPAddress.Parse(ip);

    }

    // Update is called once per frame
    public void Send(int[] heightData, int width, int i) {
        SendHeightData(heightData, width, i); 
    }

    public void SendHeightData(int[] heightData, int width, int i) {
        byte[] sendbuf = CreateBytePackage(heightData, width, i); //Encoding.ASCII.GetBytes("Nachricht Nummer: Message for Alfred: Hallo.");
        IPEndPoint ep = new IPEndPoint(broadcast, 11000);

        s.SendTo(sendbuf, ep);
       
    }

    public byte[] CreateBytePackage(int[] intArray, int width, int i) {
        byte[] outgoingArray = new byte[(intArray.Length + 2) * 2];
        byte[] widthArray = BitConverter.GetBytes((UInt16)width);
        outgoingArray[0] = widthArray[0];
        outgoingArray[1] = widthArray[1];
        byte[] iArray = BitConverter.GetBytes((UInt16)i);
        outgoingArray[2] = iArray[0];
        outgoingArray[3] = iArray[1];
        int j = 4;
        for (int index = 0; index < intArray.Length; index++) {
            byte[] heightArray = BitConverter.GetBytes((UInt16)intArray[index]);
            outgoingArray[j] = heightArray[0];
            outgoingArray[j + 1] = heightArray[1];
            j += 2;
        }
        return outgoingArray;
    }
}

