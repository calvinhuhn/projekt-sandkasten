﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

public class UDPNetworkReceiver : MonoBehaviour
{
    public GameObject mesh;

    private MeshTiles meshTiles;
    private const int listenPort = 11000;
    private Thread t;

    void Start() {
        meshTiles = mesh.GetComponent<MeshTiles>();
        ThreadStart ts = new ThreadStart(StartListener);
        t = new Thread(ts);
        t.Start();
    }

    private void StartListener() {
        UdpClient listener = new UdpClient(listenPort);
        IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, listenPort);

        try {
            while (true) {
                byte[] bytes = listener.Receive(ref groupEP);
                ReceiveFrame(bytes);
            }
        }
        catch (SocketException e) {
            Debug.Log(e);
        }
        finally {
            listener.Close();
        }
    }

    private void ReceiveFrame(byte[] received) {
        if (received != null && received[0] != 0) {
            InterpretFrame(received);
        }
    }

    private void InterpretFrame(byte[] received) {
        int width = BitConverter.ToUInt16(received, 0);
        int chunkIndex = BitConverter.ToUInt16(received, 2);

        int[] heightData = new int[(received.Length / 2) - 2];
        int i = 0;

        for (int index = 4; index < received.Length; index = index + 2) {
            heightData[i] = BitConverter.ToUInt16(received, index);
            i++;
        }

        meshTiles.ReceiveChunkUpdate(heightData, width, chunkIndex);
    }

    private void OnDestroy() {
        t.Abort();
    }
}
