﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ChunkController : MonoBehaviour {
    public GameObject meshController;
    MeshTiles meshTiles;
    public bool networkActivated;
    public string IP;
    public GameObject network;
    UDPNetworkSocket udpNetworkSocket;
    Thread thread;

    private int chunkWidth = 0;
    private int chunkHeight = 0;

    private int chunkIndexX = 0;
    private int chunkIndexY = 0;
    private int[] createdChunk;

    private void Start() {
        //meshTiles = meshController.GetComponent<MeshTiles>();
        udpNetworkSocket = network.GetComponent<UDPNetworkSocket>();
    }

    /// <summary>
    /// Sends a Package
    /// </summary>
    /// <param name="heightData"></param>
    /// <param name="deviation"></param>
    /// <param name="deviationLimit"></param>
    /// <param name="chunkDimension"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    public void SendPackage(int[] heightData, int[] deviation, int deviationLimit, int chunkDimension, int width, int height) {
        chunkWidth = width / chunkDimension;
        chunkHeight = height / chunkDimension;

        chunkIndexX = 0;
        chunkIndexY = 0;
        for (int i = 0; i < deviation.Length; i++) {
            if (deviation[i] > deviationLimit) {
                if (!networkActivated) {
                    meshTiles.ReceiveChunkUpdate(CreateChunk(i, heightData, chunkDimension, width, height, chunkIndexX, chunkIndexY), width, i);
                } else {
                   createdChunk = CreateChunk(i, heightData, chunkDimension, width, height, chunkIndexX, chunkIndexY);
                    //Debug.Log(ToString(createdChunk));
                    lock (createdChunk) {
                        if(meshTiles != null)
                        meshTiles.ReceiveChunkUpdate(createdChunk, width, i);
                        udpNetworkSocket.Send(createdChunk, width, i);
                    }
                }

            }
            chunkIndexX++;
            if (chunkIndexX >= chunkWidth) {
                chunkIndexX = 0;
                chunkIndexY++;
            }
        }
    }

    private void OnDestroy() {
        if(thread != null)
            thread.Abort();
    }

    /// <summary>
    /// Method is called, when new height frame is available.
    /// Sends all packages 
    /// 
    /// </summary>
    /// <param name="heightData"></param>
    /// <param name="deviation"></param>
    /// <param name="deviationLimit"></param>
    /// <param name="chunkDimension"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    public void NewHeightFrame(int[] heightData, int[] deviation, int deviationLimit, int chunkDimension, int width, int height) {
        if (thread != null) {
            thread.Join();
        }
        
        thread = new Thread(
            () => SendPackage(heightData, deviation, deviationLimit, chunkDimension, width, height)
        );
        thread.Start();

    }

    /// <summary>
    /// Creates a smaller chunk from the heightmap
    /// </summary>
    /// <param name="startIndex">not sure what this is doing? Probably nothing</param>
    /// <param name="heightData">current heightframe</param>
    /// <param name="chunkDimension">The chunks dimensions</param>
    /// <param name="width">number of vertecies in x</param>
    /// <param name="height">number of vertexies in y</param>
    /// <param name="chunkIndexX">current chunkIndex in direction x</param>
    /// <param name="chunkIndexY">current chunkIndex ind irection y</param>
    /// <returns></returns>
    public int[] CreateChunk(int startIndex, int[] heightData, int chunkDimension, int width, int height, int chunkIndexX, int chunkIndexY) {
        int[] chunk = new int[(chunkDimension + 1) * (chunkDimension + 1)];
        int index = 0;
        for (int y = chunkIndexY * chunkDimension; y <= chunkIndexY * chunkDimension + chunkDimension; y++) {
            for (int x = chunkIndexX * chunkDimension; x <= chunkIndexX * chunkDimension + chunkDimension; x++) {
                if (y * width + x < heightData.Length) {
                    chunk[index] = heightData[y * width + x];
                    index++;
                }
                
            }
        }
        return chunk;
    }

    public void InitTexture(ref Texture2D texture) {
        if(meshTiles != null)
        meshTiles.UpdateTexture(ref texture);
    }

    public static string ToString(int[] array) {
        string outText = "";
        foreach (byte currentByte in array) {
            outText += currentByte + ", ";
        }
        return outText;

    }

}
