﻿using UnityEngine;
using System.Collections;

public static class Bresenham {
    public static void DrawLine(Texture2D tex, Vector2 vector1, Vector2 vector2, Color col) {
        DrawLine(tex, vector1.x, vector1.y, vector2.x, vector2.y, col);
    }

    public static void DrawLine(Texture2D tex, float x0, float y0, float x1, float y1, Color col) {
        DrawLine(tex, Mathf.RoundToInt(x0), Mathf.RoundToInt(y0), Mathf.RoundToInt(x1), Mathf.RoundToInt(y1), col);
    }

    public static void DrawCircle(Texture2D tex, Vector2 vector1, float r, Color col) {
        DrawCircle(tex, vector1.x, vector1.y, r, col);
    }

    public static void DrawCircle(Texture2D tex, float x0, float y0, float r, Color col) {
        DrawCircle(tex, Mathf.RoundToInt(x0), Mathf.RoundToInt(y0), Mathf.RoundToInt(r), col);
    }

    public static void DrawLine(Texture2D tex, int x0, int y0, int x1, int y1, Color col) {
        int dy = y1 - y0;
        int dx = x1 - x0;
        int stepy, stepx, fraction;

        if (dy < 0) { dy = -dy; stepy = -1; } else { stepy = 1; }
        if (dx < 0) { dx = -dx; stepx = -1; } else { stepx = 1; }
        dy <<= 1;
        dx <<= 1;

        tex.SetPixel(x0, y0, col);
        if (dx > dy) {
            fraction = dy - (dx >> 1);
            while (x0 != x1) {
                if (fraction >= 0) {
                    y0 += stepy;
                    fraction -= dx;
                }
                x0 += stepx;
                fraction += dy;
                tex.SetPixel(x0, y0, col);
            }
        } else {
            fraction = dx - (dy >> 1);
            while (y0 != y1) {
                if (fraction >= 0) {
                    x0 += stepx;
                    fraction -= dy;
                }
                y0 += stepy;
                fraction += dx;
                tex.SetPixel(x0, y0, col);
            }
        }
        
    }

    public static void DrawCircle(Texture2D tex, int x0, int y0, int radius, Color col) {
        int f = 1 - radius;
        int ddF_x = 0;
        int ddF_y = -2 * radius;
        int x = 0;
        int y = radius;

        tex.SetPixel(x0, y0 + radius, col);
        tex.SetPixel(x0, y0 - radius, col);
        tex.SetPixel(x0 + radius, y0, col);
        tex.SetPixel(x0 - radius, y0, col);

        while (x < y) {
            if (f >= 0) {
                y--;
                ddF_y += 2;
                f += ddF_y;
            }
            x++;
            ddF_x += 2;
            f += ddF_x + 1;

            tex.SetPixel(x0 + x, y0 + y, col);
            tex.SetPixel(x0 - x, y0 + y, col);
            tex.SetPixel(x0 + x, y0 - y, col);
            tex.SetPixel(x0 - x, y0 - y, col);
            tex.SetPixel(x0 + y, y0 + x, col);
            tex.SetPixel(x0 - y, y0 + x, col);
            tex.SetPixel(x0 + y, y0 - x, col);
            tex.SetPixel(x0 - y, y0 - x, col);
        }
    }

    public static void DrawBox(Texture2D tex, int x0, int y0, int x1, int y1, Color col, int border) {
        for (int i = 0; i < border; i++) {
            Bresenham.DrawLine(tex, x0 - i, y0 - i, x1 + i, y0 - i, col);
            Bresenham.DrawLine(tex, x1 + i, y0 - i, x1 + i, y1 + i, col);
            Bresenham.DrawLine(tex, x1 + i, y1 + i, x0 - i, y1 + i, col);
            Bresenham.DrawLine(tex, x0 - i, y1 + i, x0 - i, y0 - i, col);
        }
    }


}
