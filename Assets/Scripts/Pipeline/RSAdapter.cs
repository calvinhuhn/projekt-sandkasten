﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Intel.RealSense;
using System.Runtime.InteropServices;

public class RSAdapter : MonoBehaviour
{
    public RsDevice rsDevice;
    public int distanceHighPoint;
    public int distanceLowPoint;

    //TODO Get Attributes from RS Device
    readonly int CAMERAWIDTH = 1280;
    readonly int CAMERAHEIGHT = 720;

    char[] rawHeightData;
    int[] heightOut;

   [System.Serializable]
    public enum Source {
        RealSense,
        ExampleData,
    }

    public Source source;

    public void Start() {
        rawHeightData = new char[CAMERAWIDTH * CAMERAHEIGHT];
        heightOut = new int[CAMERAHEIGHT * CAMERAWIDTH];
        if (source == Source.ExampleData) {
            GenerateSampleHeightMap();
        }
    }

    /// <summary>
    /// Event Listener for new Frames in Intel RealSense Stream
    /// </summary>
    void OnEnable() {
        //Subscribing GetCurrentDepthFrame to the RsDevice Action (OnNewSample)
        if (source == Source.RealSense) {
            rsDevice.OnNewSample += GetCurrentDepthFrame;
        }

    }

    void OnDisable() {
        //unsubscribing
        rsDevice.OnNewSample -= GetCurrentDepthFrame;
    }

    public int[] ConvertCharToIntArray(char[] arrayIn) {
        int[] arrayOut = new int[arrayIn.Length];
        for (int i = 0; i < arrayIn.Length; i++) {
            arrayOut[i] = (arrayIn[i]);
        }
        return arrayOut;
    }



    /// <summary>
    /// Each time a new Frame is beeing released, this method is beeing fired
    /// Converts the Depth Image into an float Array
    /// </summary>
    void GetCurrentDepthFrame(Frame frame) {
        //Todo Threading or c++
        var depthFrame = frame as DepthFrame;
        try {

            depthFrame.CopyTo<char>(rawHeightData);
            //Dispose 
            if ((depthFrame as Frame) != frame)
                depthFrame.Dispose();
        } catch {
            Debug.Log("Not working");
        }

        CutRawCameraData(ConvertCharToIntArray(rawHeightData), heightOut, CAMERAHEIGHT * CAMERAWIDTH, distanceHighPoint, distanceLowPoint);
    }

    

    /// <summary>
    /// Genarates a Sample Height map for testing without Depth Cameras
    /// </summary>
    private void GenerateSampleHeightMap() {

        for (int index = 0; index < CAMERAWIDTH * CAMERAHEIGHT; index++) {

            int rowIndex = (int)(index / CAMERAWIDTH);
            int columnIndex = index % CAMERAWIDTH;

            float height = Mathf.PerlinNoise(rowIndex * 0.01f, columnIndex * 0.01f) * 800;
            rawHeightData[index] = (char)(height);

        }

        CutRawCameraData(ConvertCharToIntArray(rawHeightData), heightOut, CAMERAHEIGHT * CAMERAWIDTH, distanceHighPoint, distanceLowPoint);
    }

    /// <summary>
    /// Gets the current Height Data from Capturing Device
    /// </summary>
    /// <returns>The Height Map with values between 0 (lowest Point) and 1000 (highest Point)</returns>
    public int[] GetHeightData() {
    
        
        return heightOut;
            
    }

    [DllImport("FilterDLL", EntryPoint = "cutRawCameraData")]
    public static extern void CutRawCameraData(int[] rawHeightData, int[] heightOut, int size, int distanceHighPoint, int distanceLowPoint);

    }
