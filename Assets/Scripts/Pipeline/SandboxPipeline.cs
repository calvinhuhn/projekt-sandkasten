﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Events;

public class SandboxPipeline : MonoBehaviour
{
    /* ============== Constants ================== */
    //Change your Camera Configuration here
    readonly int CAMERAWIDTH = 1280;
    readonly int CAMERAHEIGHT = 720;

    readonly private int deviationRasterWidth = 40;
    readonly private int deviationRasterHeight = 40;

    public readonly int MINDEPTH = 0;
    public readonly int MAXDEPTH = 1000;

    /* ============== public Attributes ============== */
    public RSAdapter adapter;
    public GameObject gameObject;
    public GameObject chunkController;
    
    public int width = 1280;
    public int height = 720;

    public Vector2 upperLeft;
    public Vector2 upperRight;
    public Vector2 lowerRight;
    public Vector2 lowerLeft;

    public bool lockMapActive = true;
    public bool lowPassFilterActive = true;
    [Range(0, 1)]
    public float lowpassFilterStrength = 0.5f;
    
    public int deviationLimit;
    public int deviationTolerance;

    [System.Serializable]
    public enum View {
        QuadView,
        Preview
    }
    public View viewMode = View.QuadView; 


    /* ============= private Attributes ============== */


    private int[] interpolationPositionLookupTable;

    //Arrays
    private int[] rawHeightData;
    private int[] currentHeightData;
    private byte[] emptyTextureData;
    private byte[] textureData;

    private IntPtr texturePointer;
    

    //for Lockmap
    int[] prevHeightData;
    int[] tempHeightData;
    private int[] deviationMatrix;
    private int[] refreshDeviation;
    private int[] emptyDeviationMatrix;
    private int deviationMatrixWidth;
    private int deviationMatrixHeight;
    private int[] pixelIndexToDeviationMatrixIndexLookUp;  //TODO rename


    //Textures
    private Texture2D meshTexture;
    private Texture2D previewLineTexture;

    //Eventmanager
    private bool firstframe = true;
    private int refreshCount = 10;

    public bool freeze = false;
    


    void Start()
    {
        InitParam();
        InitArrays();
        InitTextures();

        texturePointer = Marshal.AllocHGlobal(width * height * sizeof(byte));

        firstframe = true;


    }

    /* ======== Init Methods========= */
    /// <summary>
    /// Initiate Arrays
    /// </summary>
    public void InitArrays() {
        //Texture Array
        emptyTextureData = new byte[CAMERAWIDTH * CAMERAHEIGHT];
        for (int i = 0; i < emptyTextureData.Length; i++) {
            emptyTextureData[i] = 0;
        }
        textureData = new byte[width * height];

        //Heightmap for raw heightdata
        rawHeightData = new int[CAMERAHEIGHT * CAMERAWIDTH];

        //current heightdata for filters
        currentHeightData = new int[height * width];

        //prev heightdata used for lowepass and lockmap 
        prevHeightData = new int[height * width];
        tempHeightData = new int[height * width];

        //Interpolation
        interpolationPositionLookupTable = new int[height * width];
        CreateInterpolationLookUp(upperLeft, upperRight, lowerRight, lowerLeft);
      

        //Lockmap
        deviationMatrix = new int[deviationMatrixHeight * deviationMatrixWidth];
        refreshDeviation = new int[deviationMatrixHeight * deviationMatrixWidth];
        for (int i = 0; i < refreshDeviation.Length; i++) {
            refreshDeviation[i] = int.MaxValue;
        }
        emptyDeviationMatrix = new int[deviationMatrixHeight * deviationMatrixWidth];
        pixelIndexToDeviationMatrixIndexLookUp = new int[height * width];
        CreateDeviationLookUpKoordinates(ref pixelIndexToDeviationMatrixIndexLookUp);
    }

    public void InitParam() {
        deviationMatrixHeight = height / deviationRasterHeight;
        deviationMatrixWidth = width / deviationRasterWidth;
    }

    public void InitTextures() {
        meshTexture = new Texture2D(width, height, TextureFormat.R8, false);
        previewLineTexture = new Texture2D(CAMERAWIDTH, CAMERAHEIGHT, TextureFormat.Alpha8, false);
    }

    void Update() {
        //Get the latest heightdata from the RealSense Adapter
        adapter.GetHeightData().CopyTo(rawHeightData, 0);
        KeyEvents();
        //Skip if freezes
        if (!freeze) {
            if (viewMode == View.QuadView) {
                DoFilterChain();
            } else {
                CreatePreview();
            }
        }
       

        if (Input.GetKeyUp(KeyCode.F)) {
            lockMapActive = !lockMapActive;
        }
    }

    /* ============= Filter Chain =============== */

    /// <summary>
    /// Main method for filtering the realsense heightdata. 
    /// Includes: lockmap, lowpass, resize, building textures, assining texture
    /// </summary>
    void DoFilterChain() {

       
        //reduce dimensions and cut to area
        InterpolateWithLookUp(rawHeightData, currentHeightData, interpolationPositionLookupTable, height, width);

        //LockMap
        if ((lockMapActive && refreshCount <= 0) || Input.GetKeyUp(KeyCode.R)) {
            refreshCount = 10000;
            Copy(currentHeightData, ref prevHeightData);
            Copy(refreshDeviation, ref deviationMatrix);
          


        } else {

            if (lockMapActive) {
                Copy(emptyDeviationMatrix, ref deviationMatrix);
                CreateDeviationMatrix(currentHeightData, prevHeightData, deviationMatrix, pixelIndexToDeviationMatrixIndexLookUp, currentHeightData.Length, deviationTolerance, deviationMatrixWidth, deviationRasterWidth, deviationRasterHeight);

                ApplyLockMap(currentHeightData, prevHeightData, currentHeightData.Length, deviationMatrix, pixelIndexToDeviationMatrixIndexLookUp, deviationMatrixWidth, deviationLimit, deviationRasterWidth, deviationRasterHeight);
                refreshCount--;
            }

            //lowPass
            if (lowPassFilterActive) {
                ApplyLowPass(prevHeightData, currentHeightData, currentHeightData.Length, lowpassFilterStrength);
            }

            if (lockMapActive || lowPassFilterActive) {
                Copy(currentHeightData, ref prevHeightData);
            }
        }

       

        //Build Texture
        BuildTexture(currentHeightData, width, height, ref meshTexture);

        //AssignTexture
        AssignTexture(gameObject, meshTexture);
        chunkController.GetComponent<ChunkController>().NewHeightFrame(currentHeightData, deviationMatrix, deviationLimit, deviationRasterHeight, width, height);
       
        chunkController.GetComponent<ChunkController>().InitTexture(ref meshTexture);

        firstframe = false;
    }

    /// <summary>
    /// Creates heightmap preview texture 
    /// </summary>
    void CreatePreview() {
        InterpolateBilinear(rawHeightData, currentHeightData, width, height, CAMERAWIDTH, CAMERAHEIGHT, 0, CAMERAHEIGHT, CAMERAWIDTH, CAMERAHEIGHT, CAMERAWIDTH, 0, 0, 0);
        // InterpolateBilinear(rawHeightData, currentHeightData, width, height, CAMERAWIDTH, CAMERAHEIGHT, CAMERAWIDTH, CAMERAHEIGHT, 0, CAMERAHEIGHT, 0, 0, CAMERAWIDTH, 0 );
        BuildTexture(currentHeightData, width, height, ref meshTexture);
    }

    /* =============Filter ============= */

    /// <summary>
    /// </summary>
    /// <param name="upperLeft"></param>
    /// <param name="upperLeft"></param>
    /// <param name="upperRight"></param>
    /// <param name="lowerRight"></param>
    /// <param name="lowerLeft"></param>
    private void CreateInterpolationLookUp(Vector2 lowerLeft, Vector2 lowerRight,  Vector2 upperRight, Vector2 upperLeft) {
        upperLeft.y = CAMERAHEIGHT - upperLeft.y;
        upperRight.y = CAMERAHEIGHT - upperRight.y;
        lowerRight.y = CAMERAHEIGHT - lowerRight.y;
        lowerLeft.y = CAMERAHEIGHT - lowerLeft.y;
        //Startpoint
        Vector2 aStart = upperLeft;

        //Directions:
        Vector2 ab = lowerLeft - upperLeft;

        Vector2 ad = upperRight - upperLeft;
        Vector2 bc = lowerRight - lowerLeft;

        // new Target Array with corresponding size
        interpolationPositionLookupTable = new int[height * width];
        for (int targetX = 0; targetX < width; targetX++) {
            for (int targetY = 0; targetY < height; targetY++) {

                //Fraction on Y  
                float fractionAB = ((float)targetY / height);
                float fractionADBC = ((float)targetX / width);


                Vector2 G = fractionAB * lowerLeft + (1 - fractionAB) * upperLeft;
                Vector2 H = fractionAB * lowerRight + (1 - fractionAB) * upperRight;
                Vector2 resultPoint = fractionADBC * H + (1 - fractionADBC) * G;

                //TODO Look at four different neighbour pixels and interpolate
                float positionX = resultPoint.x;
                float positionY = resultPoint.y;

                int n = Mathf.FloorToInt(positionX);   //  X
                int m = Mathf.FloorToInt(positionY);   //  Y
               

                //Calculate a, b, c, d. If a is a Pixel in a last Row or/and Column, the next pixel is getting the same value. 
                if (n < CAMERAWIDTH && m < CAMERAHEIGHT) {

                    interpolationPositionLookupTable[targetX + targetY * width] = n + m * CAMERAWIDTH ;
                }
            }
        }
    }

    /// <summary>
    /// Creates the 1D coordinates for the deviation lookup table
    /// </summary>
    /// <param name="deviationMatrixIndexLookUp"></param>
    public void CreateDeviationLookUpKoordinates(ref int[] deviationMatrixIndexLookUp) {
        int deviationMatrixX = 0;
        int deviationMatriyY = 0;
        int x = 0;
        int y = 0;
        for (int i = 0; i < deviationMatrixIndexLookUp.Length; i++) {
            deviationMatrixIndexLookUp[i] = deviationMatrixX + deviationMatriyY * deviationMatrixWidth;
            if (x >= deviationRasterWidth) {
                deviationMatrixX++;
                x = 0;
            }
            if (deviationMatrixX >= deviationMatrixWidth) {
                y++;
                if (y >= deviationRasterHeight) {
                    deviationMatriyY++;
                    y = 0;
                }
                deviationMatrixX = 0;
            }
            x++;
            //Increase
        }
    }


    /* ============= utils  =========== */
    /// <summary>
    /// Copys two Arrays from the same size.
    /// </summary>
    /// <param name="source">Source Array</param>
    /// <param name="target">Target Array</param>
    public void Copy(int[] source, ref int[] target) {
        for (int i = 0; i < source.Length; i++) {
            target[i] = source[i];
        }
    }

    /*============== Textures ================ */
    /// <summary>
    /// Builds height texture on R8 texture.
    /// </summary>
    /// <param name="heightData"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="texture"></param>
    private void BuildTexture(int[] heightData, int width, int height, ref Texture2D texture) {
       
        GenerateTextureByteArray(texturePointer, heightData, heightData.Length);
        Marshal.Copy(texturePointer, textureData, 0, heightData.Length);


        texture.LoadRawTextureData(textureData);
        texture.Apply();
    }

    private void AssignTexture(GameObject g, Texture2D t) {
        g.GetComponent<MeshRenderer>().material.mainTexture = t;
    }

    /* ============= OnGui ============= */
    public void OnGUI() {
        if (viewMode == View.Preview) {
            GUI.DrawTexture(new Rect(0, Screen.height - CAMERAHEIGHT, CAMERAWIDTH, CAMERAHEIGHT), meshTexture);
            GUI.DrawTexture(new Rect(0, Screen.height - CAMERAHEIGHT, CAMERAWIDTH, CAMERAHEIGHT), previewLineTexture);
        }
    }


    /// <summary>
    /// Checks for Key Events
    /// </summary>
    public void KeyEvents() {
        if (Input.GetKeyUp(KeyCode.Space)) {
            if (viewMode == View.Preview) {
                viewMode = View.QuadView;
            } else {
                viewMode = View.Preview;
                DrawPreviewLines();
            }
        }

        if (Input.GetKeyUp(KeyCode.Keypad7) || Input.GetKeyUp(KeyCode.Alpha7)) {
            upperLeft = Input.mousePosition;
          
            CreateInterpolationLookUp(upperLeft, upperRight, lowerRight, lowerLeft);
            DrawPreviewLines();
        } else if (Input.GetKeyUp(KeyCode.Keypad9)) {
            upperRight = Input.mousePosition;
           
            CreateInterpolationLookUp(upperLeft, upperRight, lowerRight, lowerLeft);
            DrawPreviewLines();
        } else if (Input.GetKeyUp(KeyCode.Keypad3)) {
            lowerRight = Input.mousePosition;
           
            CreateInterpolationLookUp(upperLeft, upperRight, lowerRight, lowerLeft);
            DrawPreviewLines();
        } else if (Input.GetKeyUp(KeyCode.Keypad1)) {
            lowerLeft = Input.mousePosition;
           
            CreateInterpolationLookUp(upperLeft, upperRight, lowerRight, lowerLeft);
            DrawPreviewLines();
        }
    }

    public void DrawPreviewLines() {

        //Draw All Lines
        previewLineTexture.LoadRawTextureData(emptyTextureData);
        Bresenham.DrawLine(previewLineTexture, upperLeft.x, upperLeft.y, upperRight.x, upperRight.y, new Color(1, 1, 1, 1));
        Bresenham.DrawLine(previewLineTexture, upperRight.x, upperRight.y, lowerRight.x, lowerRight.y, new Color(1, 1, 1, 1));
        Bresenham.DrawLine(previewLineTexture, lowerRight.x, lowerRight.y, lowerLeft.x, lowerLeft.y, new Color(1, 1, 1, 1));
        Bresenham.DrawLine(previewLineTexture, lowerLeft.x, lowerLeft.y, upperLeft.x, upperLeft.y, new Color(1, 1, 1, 1));
        previewLineTexture.Apply();
    }


    /* ======= External Dlls ========= */

    [DllImport("FilterDLL", EntryPoint = "GenerateTextureByteArray")]
    public static extern void GenerateTextureByteArray(IntPtr textureData, int[] heightData, int size);

    [DllImport("FilterDLL", EntryPoint = "InterpolateBilinear")]
    public static extern void InterpolateBilinear(int[] source, int[] target, int width, int height, int CAMERAWIDTH, int CAMERAHEIGHT, int upperLeftX, int upperLeftY, int upperRightX, int upperRightY, int lowerRightX, int lowerRightY, int lowerLeftX, int lowerLeftY);

    [DllImport("FilterDLL", EntryPoint = "InterpolateWithLookUp")]
    public static extern void InterpolateWithLookUp(int[] source, int[] target, int[] interpolationPositionLockupTable, int height, int width);

    [DllImport("FilterDLL", EntryPoint = "CreateDeviationMatrix")]
    public static extern void CreateDeviationMatrix(int[] current, int[] prev, int[] deviationMatrix, int[] pixelIndexToDeviationMatrixIndexLookUp, int size, int deviationTolerance, int deviationMatrixWidth, int deviationRasterWidth, int deviationRasterHeight);

    [DllImport("FilterDLL", EntryPoint = "ApplyLowPass")]
    public static extern void ApplyLowPass(int[] source, int[] target, int arraySize, float lowpassFilterStrength);

    [DllImport("FilterDLL", EntryPoint = "ApplyLockMap")]
    public static extern void ApplyLockMap(int[] current, int[] prev, int arraySize, int[] deviationMatrix, int[] pixelIndexToDeviationMatrixIndexLookUp, int deviationMatrixWidth, int deviationLimit, int deviationRasterWidth, int deviationRasterHeight);

}
