﻿Shader "Custom/lp_shader"
{
    Properties
    {
        _MainTex ("Albedo (RGB)", 2D) = "white" {}

		_FirstColor ("Color 1", Color) = (1.0, 1.0, 1.0, 1.0)
		_FirstMin ("Color 1 Min", Range(0,1)) = 0.0
		_FirstMax ("Color 1 Max", Range(0,1)) = 0.198
		_FirstGlossiness ("Color 1 Smoothness", Range(0,1)) = 0.5
		_FirstMetallic ("Color 1 Metallic", Range(0,1)) = 0.0
		
		_SecondColor ("Color 2", Color) = (1.0, 1.0, 1.0, 1.0)
		_SecondMin ("Color 2 Min", Range(0,1)) = 0.323
		_SecondMax ("Color 2 Max", Range(0,1)) = 0.354
		_SecondGlossiness ("Color 2 Smoothness", Range(0,1)) = 0.5
		_SecondMetallic ("Color 2 Metallic", Range(0,1)) = 0.0
		
		_ThirdColor ("Texture 3 Tint", Color) = (1.0, 1.0, 1.0, 1.0)
		_ThirdMin ("Texture 3 Min", Range(0,1)) = 0.458
		_ThirdMax ("Texture 3 Max", Range(0,1)) = 0.458
		_ThirdGlossiness ("Color 3 Smoothness", Range(0,1)) = 0.5
		_ThirdMetallic ("Color 3 Metallic", Range(0,1)) = 0.0
		
		_FourthColor ("Texture 4 Tint", Color) = (1.0, 1.0, 1.0, 1.0)
		_FourthMin ("Texture 4 Min", Range(0,1)) = 0.748
		_FourthMax ("Texture 4 Max", Range(0,1)) = 0.890
		_FourthGlossiness ("Color 4 Smoothness", Range(0,1)) = 0.5
		_FourthMetallic ("Color 4 Metallic", Range(0,1)) = 0.0
		
		_FifthColor ("Texture 5 Tint", Color) = (1.0, 1.0, 1.0, 1.0)
		_FifthMin ("Texture 5 Min", Range(0,1)) = 0.950
		_FifthMax ("Texture 5 Max", Range(0,1)) = 1.0
		_FifthGlossiness ("Color 4 Smoothness", Range(0,1)) = 0.5
		_FifthMetallic ("Color 4 Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        //#pragma surface surf Standard fullforwardshadows
		#pragma surface surf Standard vertex:vert fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
			float4 vertColor;
        };
        
		float _FirstMin;
		float _FirstMax;
		fixed4 _FirstColor;
		half _FirstGlossiness;
		half _FirstMetallic;

		float _SecondMin;
		float _SecondMax;
		fixed4 _SecondColor;
		half _SecondGlossiness;
		half _SecondMetallic;

		float _ThirdMin;
		float _ThirdMax;
		fixed4 _ThirdColor;
		half _ThirdGlossiness;
		half _ThirdMetallic;

		float _FourthMin;
		float _FourthMax;
		fixed4 _FourthColor;
		half _FourthGlossiness;
		half _FourthMetallic;

		float _FifthMin;
		float _FifthMax;
		fixed4 _FifthColor;
		half _FifthGlossiness;
		half _FifthMetallic;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

		void vert(inout appdata_full v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.vertColor = v.color;
		}

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            //o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            //o.Metallic = _Metallic;
            //o.Smoothness = _Glossiness;
            //o.Alpha = c.a;
			//o.Albedo = IN.vertColor.rgb;
			
			if(c.r > _FifthMin) {
				o.Albedo = _FifthColor.rgb * IN.vertColor.rgb;
			//} else if(c.r > _FourthMax) {
				
				
			} else if(c.r > _FourthMin) {
				o.Albedo = _FourthColor.rgb * IN.vertColor.rgb;
			//} else if(c.r > _ThirdMax) {
				
			} else if(c.r > _ThirdMin) {
				o.Albedo = _ThirdColor.rgb * IN.vertColor.rgb;
			//} else if(c.r > _SecondMax) {
				
			} else if(c.r > _SecondMin) {
				o.Albedo = _SecondColor.rgb * IN.vertColor.rgb;
			//} else if(c.r > _FirstMax) {
				
			} else {
				o.Albedo = _FirstColor.rgb * IN.vertColor.rgb;
			} 
			
        }
        ENDCG
    }
    FallBack "Diffuse"
}
