﻿Shader "Custom/map_shader"
{
    Properties
    {
		_MainTex ("Texture Map", 2D) = "white" {}

		_FirstTex ("Texture 1", 2D) = "white" {}
		_FirstTint ("Texture 1 Tint", Color) = (1.0, 1.0, 1.0, 1.0)
		_FirstMin ("Texture 1 Min", Range(0,1)) = 0.0
		_FirstMax ("Texture 1 Max", Range(0,1)) = 0.198
		
		_SecondTex ("Texture 2", 2D) = "white" {}
		_SecondTint ("Texture 2 Tint", Color) = (1.0, 1.0, 1.0, 1.0)
		_SecondMin ("Texture 2 Min", Range(0,1)) = 0.323
		_SecondMax ("Texture 2 Max", Range(0,1)) = 0.354
		
		_ThirdTex ("Texture 3", 2D) = "white" {}
		_ThirdTint ("Texture 3 Tint", Color) = (1.0, 1.0, 1.0, 1.0)
		_ThirdMin ("Texture 3 Min", Range(0,1)) = 0.458
		_ThirdMax ("Texture 3 Max", Range(0,1)) = 0.458
		
		_FourthTex ("Texture 4", 2D) = "white" {}
		//_FourthTexH ("Texture 4 Height", 2D) = "white" {}
		_FourthTint ("Texture 4 Tint", Color) = (1.0, 1.0, 1.0, 1.0)
		_FourthMin ("Texture 4 Min", Range(0,1)) = 0.748
		_FourthMax ("Texture 4 Max", Range(0,1)) = 0.890
		
		_FifthTex ("Texture 5", 2D) = "white" {}
		//_FifthTexH ("Texture 5 Height", 2D) = "white" {}
		_FifthTint ("Texture 5 Tint", Color) = (1.0, 1.0, 1.0, 1.0)
		_FifthMin ("Texture 5 Min", Range(0,1)) = 0.950
		_FifthMax ("Texture 5 Max", Range(0,1)) = 1.0
		
		

		/*_SnowCol ("Snow Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_SnowHeight ("Snow Height", Range (0, 1)) = 0.97
		_RockCol ("Rock Color", Color) = (0.5943396, 0.5664805, 0.4906105, 1.0)
		_RockHeight ("Rock Height", Range (0, 1)) = 0.7
		_GrassCol ("Grass Color", Color) = (0.4910159, 0.6981132, 0.1020826, 1.0)
		_GrassHeight ("Grass Height", Range (0, 1)) = 0.35
		_SandCol ("Sand Color", Color) = (0.8962264, 0.8612899, 0.5876201, 1.0)
		_SandHeight ("Sand Height", Range (0, 1)) = 0.29
		_WaterCol ("Water Color", Color) = (0.0, 0.5646706, 1.0, 1.0)
		_WaterHeight ("Water Height", Range (0, 1)) = 0.0*/
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma target 3.0

			#include "noiseSimplex.cginc"
            #include "UnityCG.cginc"
			

			/*
			fixed4 _SnowCol;
			float _SnowHeight;
			fixed4 _RockCol;
			float _RockHeight;
			fixed4 _GrassCol;
			float _GrassHeight;
			fixed4 _SandCol;
			float _SandHeight;
			fixed4 _WaterCol;
			float _WaterHeight;
			*/

			sampler2D _MainTex;
            float4 _MainTex_ST;

			float _FirstMin;
			float _FirstMax;
			fixed4 _FirstTint;
			sampler2D _FirstTex;
			float4 _FirstTex_ST;

			float _SecondMin;
			float _SecondMax;
			fixed4 _SecondTint;
			sampler2D _SecondTex;
			float4 _SecondTex_ST;

			float _ThirdMin;
			float _ThirdMax;
			fixed4 _ThirdTint;
			sampler2D _ThirdTex;
			float4 _ThirdTex_ST;

			float _FourthMin;
			float _FourthMax;
			fixed4 _FourthTint;
			sampler2D _FourthTex;
			float4 _FourthTex_ST;
			//sampler2D _FourthTexH;
			//float4 _FourthTexH_ST;

			float _FifthMin;
			float _FifthMax;
			fixed4 _FifthTint;
			sampler2D _FifthTex;
			float4 _FifthTex_ST;
			//sampler2D _FifthTexH;
			//float4 _FifthTexH_ST;


            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

			float4 lerp(float4 a, float4 b, float t) 
			{
				return (t) * a + (1 - t) * b;
			}

			float map (float value, float from1, float to1, float from2, float to2) {
				return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
			}

			float4 hash4( float2 p )
			{
				return frac(sin(float4( 1.0+dot(p,float2(37.0,17.0)), 2.0+dot(p,float2(11.0,47.0)), 3.0+dot(p,float2(41.0,29.0)), 4.0+dot(p,float2(23.0,31.0))))*103.0);
			}

			float4 textureNoTile( sampler2D samp, in float2 uv )
			{
				float2 p = floor( uv );
				float2 f = frac( uv );
	
				// derivatives (for correct mipmapping)
				float2 _ddx = ddx( uv );
				float2 _ddy = ddy( uv );
    
				// voronoi contribution
				float4 va = float4( 0.0, 0.0, 0.0, 0.0 );
				float wt = 0.0;
				for( int j=-1; j<=1; j++ )
				for( int i=-1; i<=1; i++ )
				{
					float2 g = float2( float(i), float(j) );
					float4 o = hash4( p + g );
					float2 r = g - f + o.xy;
					float d = dot(r,r);
					float w = exp(-5.0*d );
					float4 c = tex2D( samp, uv + o.zw, _ddx, _ddy );
					va += w*c;
					wt += w;
				}
	
				// normalization
				return va/wt;
			}

            fixed4 frag (v2f i) : SV_Target
            {
				float2 main_uv = TRANSFORM_TEX(i.uv, _MainTex);
				float2 first_uv = TRANSFORM_TEX(i.uv, _FirstTex);
				float2 second_uv = TRANSFORM_TEX(i.uv, _SecondTex);
				float2 third_uv = TRANSFORM_TEX(i.uv, _ThirdTex);
				float2 fourth_uv = TRANSFORM_TEX(i.uv, _FourthTex);
				float2 fifth_uv = TRANSFORM_TEX(i.uv, _FifthTex);

				fixed4 blend_color = tex2D(_MainTex, main_uv) + (snoise(main_uv / 0.07) / 100) + (snoise(main_uv / 0.1) / 80) + (snoise(main_uv / 0.2) / 600);
				fixed4 first_color = textureNoTile(_FirstTex, first_uv);
				fixed4 second_color = textureNoTile(_SecondTex, second_uv);
				fixed4 third_color = textureNoTile(_ThirdTex, third_uv);
				fixed4 fourth_color = textureNoTile(_FourthTex, fourth_uv);
				fixed4 fifth_color = textureNoTile(_FifthTex, fifth_uv);

				//float first_h_color = dot(first_color.rgb, float3(0.3, 0.59, 0.11)).r;
				//float second_h_color = dot(second_color.rgb, float3(0.3, 0.59, 0.11)).r;
				//float third_h_color = dot(third_color.rgb, float3(0.3, 0.59, 0.11)).r;
				//float fourth_h_color = textureNoTile(_FourthTexH, fourth_uv).r;
				//float fifth_h_color = textureNoTile(_FifthTexH, fifth_uv).r;

				//fixed4 blend_color = tex2D(_MainTex, main_uv);
				//fixed4 first_color = tex2D(_FirstTex, first_uv);
				//fixed4 second_color = tex2D(_SecondTex, second_uv);
				//fixed4 third_color = tex2D(_ThirdTex, third_uv);
				//fixed4 fourth_color = tex2D(_FourthTex, fourth_uv);
				//fixed4 fifth_color = tex2D(_FifthTex, fifth_uv);

				fixed4 col = blend_color;

				//if(blend_color.r > _FifthMax) {
				//	col = lerp((1.0, 1.0, 1.0, 0.0), _FifthTint * fifth_color, 0 + (blend_color.r - _FifthMax) * (1 - 0) / (1 - _FifthMax));
				//} else
				if(blend_color.r > _FifthMin) {
					col = _FifthTint * fifth_color;
				} else if(blend_color.r > _FourthMax) {
					//float2 _col = float2(map(blend_color.r, _FourthMax, _FifthMin, 0, 1) + fourth_h_color, map(blend_color.r, _FourthMax, _FifthMin, 1, 0) + fifth_h_color);
					//float t1 = (1 / (1 * pow(2, _col.r)) + 1) / 2;
					//float t2 = (1 / (1 * pow(2, _col.g)) + 1) / 2;
					//float sum = t1 + t2;
					//t1 /= sum;
					//t2 /= sum;
					//fourth_color *= t1;
					//fifth_color *= t2;
					//col = fourth_color + fifth_color;
					col = lerp(_FifthTint * fifth_color, _FourthTint * fourth_color, map(blend_color.r, _FourthMax, _FifthMin, 0, 1));
				} else if(blend_color.r > _FourthMin) {
					col = _FourthTint * fourth_color;
				} else if(blend_color.r > _ThirdMax) {
					col = lerp(_FourthTint * fourth_color, _ThirdTint * third_color, map(blend_color.r, _ThirdMax, _FourthMin, 0, 1));
				} else if(blend_color.r > _ThirdMin) {
					col = _ThirdTint * third_color;
				} else if(blend_color.r > _SecondMax) {
					col = lerp(_ThirdTint * third_color, _SecondTint * second_color, map(blend_color.r, _SecondMax, _ThirdMin, 0, 1));
				} else if(blend_color.r > _SecondMin) {
					col = _SecondTint * second_color;
				} else if(blend_color.r > _FirstMax) {
					col = lerp(_SecondTint * second_color, _FirstTint * first_color, map(blend_color.r, _FirstMax, _SecondMin, 0, 1));
				} else {
					col = _FirstTint * first_color;
				} 
				//else {
				//	col = lerp(_FirstTint * first_color, float4(1.0, 1.0, 1.0, 0.0), 0 + (blend_color.r - 0) * (1 - 0) / (_FirstMin - 0));
				//}
				
				return col;

				/*
				fixed4 blend;
                fixed4 col = tex2D(_MainTex, i.uv);
				if(col.r > _SnowHeight) {
					col = _SnowCol;
				} else if (col.r > _RockHeight + 0.15) {
					blend = 0 + (col.r - (_RockHeight + 0.15)) * (1 - 0) / (_SnowHeight - (_RockHeight + 0.15));
					col = lerp(_SnowCol, _RockCol, 1 - blend);
				} else if(col.r > _RockHeight) {
					col = _RockCol;
				} else if(col.r > _GrassHeight + 0.05) {
					blend = 0 + (col.r - (_GrassHeight + 0.05)) * (1 - 0) / (_RockHeight - (_GrassHeight + 0.05));
					col = lerp(_RockCol, _GrassCol, 1 - blend);
				} else if(col.r > _GrassHeight) {
					col = _GrassCol;
				} else if(col.r > _SandHeight + 0.01) {
					blend = 0 + (col.r - (_SandHeight + 0.01)) * (1 - 0) / (_GrassHeight - (_SandHeight + 0.01));
					col = lerp(_GrassCol, _SandCol, 1 - blend);
				} else if(col.r > _SandHeight) {
					col = _SandCol;
				} else if(col.r > _WaterHeight + 0.25) {
					blend = 0 + (col.r - (_WaterHeight + 0.25)) * (1 - 0) / (_SandHeight - (_WaterHeight + 0.25));
					col = lerp(_SandCol, _WaterCol, 1 - blend);
				} else if(col.r >= 0.0) {
					col = _WaterCol;
				}
                return col;
				*/
            }
            ENDCG
        }
    }
}
