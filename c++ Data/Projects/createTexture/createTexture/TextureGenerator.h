#define TEST_API _declspec(dllexport)
extern "C" {
	TEST_API void GenerateByte(unsigned char texture[], int heightData[], int width, int height, float minDepth, float maxDepth);
	TEST_API void InterpolateBilinear(int source[], int target[], int width, int height, int CAMERAWIDTH, int CAMERAHEIGHT, int upperLeftX, int upperLeftY, int upperRightX, int upperRightY, int lowerRightX, int lowerRightY, int lowerLeftX, int lowerLeftY);
}
