#include "stdafx.h"
#include "TextureGenerator.h"
#include <algorithm>

extern "C" {

	void GenerateByte(unsigned char texture[], int heightData[], int width, int height, float minDepth, float maxDepth)
	{

		for (int index = 0; index < width * height; index++) {
			int currentheight = heightData[index];
			//   return (byte)(255 - (height / (float)1000) * byte.MaxValue);
			*texture = (unsigned char)((255 - (height / (float)1000) * byte.MaxValue);
			++texture;
		}


	}

	// public static extern byte[] InterpolateBilinear(int[] source, int[] target, int upperLeftX, int upperLeftY, int upperRightX, int upperRightY, int lowerRightX, int lowerRightY, int lowerLeftX, int lowerLeftY);
	//InterpolateBilinear(int[] source, int[] target,  Vector2 upperLeft, Vector2 upperRight, Vector2 lowerRight, Vector2 lowerLeft)
	void InterpolateBilinear(int source[], int target[], int width, int height, int CAMERAWIDTH, int CAMERAHEIGHT, int upperLeftX, int upperLeftY, int upperRightX, int upperRightY, int lowerRightX, int lowerRightY, int lowerLeftX, int lowerLeftY) {

		for (int targetX = 0; targetX < width; targetX++) {
			for (int targetY = 0; targetY < height; targetY++) {

				//Fractions
				float fractionAB = ((float)targetY / height);
				float fractionADBC = ((float)targetX / width);


				float Gx = fractionAB * lowerLeftX + (1 - fractionAB) * upperLeftX;
				float Gy = fractionAB * lowerLeftY + (1 - fractionAB) * upperLeftY;

				float Hx = fractionAB * lowerRightX + (1 - fractionAB) * upperRightX;
				float Hy = fractionAB * lowerRightY + (1 - fractionAB) * upperRightY;

				float resultPointX = fractionADBC * Hx + (1 - fractionADBC) * Gx;
				float resultPointY = fractionADBC * Hy + (1 - fractionADBC) * Gy;

				
				float positionX = resultPointX;
				float positionY = resultPointY;

				int n = floor(positionX);   //  X
				int m = floor(positionY);   //  Y
				float v = positionX - n;  //  X
				float h = positionY - m;  //  Y  

										  //Calculate a, b, c, d. If a is a Pixel in a last Row or/and Column, the next pixel is getting the same value. 
				if (n < CAMERAWIDTH && m < CAMERAHEIGHT) {

					float a = source[n + m * CAMERAWIDTH];

					target[targetX + targetY * width] = (int)(a);
				}


			}
			
		}
	}

}
