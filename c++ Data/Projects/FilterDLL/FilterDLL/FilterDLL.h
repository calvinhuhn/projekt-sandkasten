#define FILTER_API __declspec(dllexport)
extern "C" {
	FILTER_API void GenerateTextureByteArray(unsigned char texture[], int heightData[], int size);
	FILTER_API void InterpolateWithLookUp(int source[], int target[], int interpolationPositionLockupTable[], int height, int width);
	FILTER_API void ApplyLowPass(int source[], int target[], int arraySize, float lowpassFilterStrength);
	FILTER_API void CreateDeviationMatrix(int current[], int prev[], int deviationMatrix[], int pixelIndexToDeviationMatrixIndexLookUp[], int size, int deviationTolerance, int deviationMatrixWidth, int deviationRasterWidth, int deviationRasterHeight);
	FILTER_API void ApplyLockMap(int current[], int prev[], int arraySize, int deviationMatrix[], int pixelIndexToDeviationMatrixIndexLookUp[], int deviationMatrixWidth, int deviationLimit, int deviationRasterWidth, int deviationRasterHeight);
	FILTER_API void InterpolateBilinear(int source[], int target[], int width, int height, int camerawidth, int cameraheight, int upperLeftX, int upperLeftY, int upperRightX, int upperRightY, int lowerRightX, int lowerRightY, int lowerLeftX, int lowerLeftY);
	FILTER_API void CutRawCameraData(unsigned short rawHeightData[], int heightOut[], int size, int distanceHighPoint, int distanceLowPoint);
}