#include "stdafx.h"
#include "FilterDll.h"
#include <algorithm>

extern "C" {

	void GenerateTextureByteArray(unsigned char texture[], int heightData[], int size)
	{
		for (int index = 0; index < size; index++) {
			*texture = (unsigned char)((*heightData / (float)1000) * 255);
			++texture;
			++heightData;
		}
	}

	void InterpolateWithLookUp(int source[], int target[], int interpolationPositionLockupTable[], int height, int width) {
		int index = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				target[index] = source[interpolationPositionLockupTable[index]];
				index++;
			}
		}
	}

	void InterpolateBilinear(int source[], int target[], int width, int height, int camerawidth, int cameraheight, int upperLeftX, int upperLeftY, int upperRightX, int upperRightY, int lowerRightX, int lowerRightY, int lowerLeftX, int lowerLeftY) {

		for (int targetX = 0; targetX < width; targetX++) {
			for (int targetY = 0; targetY < height; targetY++) {

				//Fractions
				float fractionAB = ((float)targetY / height);
				float fractionADBC = ((float)targetX / width);


				float Gx = fractionAB * lowerLeftX + (1 - fractionAB) * upperLeftX;
				float Gy = fractionAB * lowerLeftY + (1 - fractionAB) * upperLeftY;

				float Hx = fractionAB * lowerRightX + (1 - fractionAB) * upperRightX;
				float Hy = fractionAB * lowerRightY + (1 - fractionAB) * upperRightY;

				float resultPointX = fractionADBC * Hx + (1 - fractionADBC) * Gx;
				float resultPointY = fractionADBC * Hy + (1 - fractionADBC) * Gy;


				float positionX = resultPointX;
				float positionY = resultPointY;

				int n = floor(positionX);   //  X
				int m = floor(positionY);   //  Y
				float v = positionX - n;  //  X
				float h = positionY - m;  //  Y  

										  //Calculate a, b, c, d. If a is a Pixel in a last Row or/and Column, the next pixel is getting the same value. 
				if (n < camerawidth && m < cameraheight) {

					float a = source[n + m * camerawidth];

					target[targetX + targetY * width] = (int)(a);

				}
			}
		}
	}

	void ApplyLowPass(int source[], int target[], int arraySize, float lowpassFilterStrength) {
		for (int i = 0; i < arraySize; i++) {
			target[i] = (int)((float)(1 - lowpassFilterStrength) * target[i] + (float)(lowpassFilterStrength)* source[i]);
		}
	}


	void CreateDeviationMatrix(int current[], int prev[], int deviationMatrix[], int pixelIndexToDeviationMatrixIndexLookUp[], int size, int deviationTolerance, int deviationMatrixWidth, int deviationRasterWidth, int deviationRasterHeight) {
	
		for (int i = 0; i < size; i++) {
			if (abs(current[i] - prev[i]) > deviationTolerance) {
				deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i]] = deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i]] + 1;
				
				if ((pixelIndexToDeviationMatrixIndexLookUp[i] - deviationMatrixWidth > 0 ) && (pixelIndexToDeviationMatrixIndexLookUp[i] + deviationMatrixWidth < size)) {

					deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] + 1] = deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] + 1] + 1;
					deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] - 1] = deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] - 1] + 1;
					deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] + deviationMatrixWidth] = deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] + deviationMatrixWidth] + 1;
					deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] - deviationMatrixWidth] = deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] - deviationMatrixWidth] + 1;

				}
				
				/*

				deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] + 1] = deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] + 1] + 1;
				deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] - 1] = deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] - 1] + 1;
				deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] + deviationMatrixWidth] = deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] + deviationMatrixWidth] + 1;
				deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] - deviationMatrixWidth] = deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i] - deviationMatrixWidth] + 1;
				*/
			}
	
		}
	}


	void ApplyLockMap(int current[], int prev[], int arraySize, int deviationMatrix[], int pixelIndexToDeviationMatrixIndexLookUp[],  int deviationMatrixWidth, int deviationLimit, int deviationRasterWidth, int deviationRasterHeight) {
		
		for (int i = 0; i < arraySize; i++) {

			if (deviationMatrix[pixelIndexToDeviationMatrixIndexLookUp[i]] < deviationLimit) {
				current[i] = prev[i];
			}


		}
		

	}
	
	void CutRawCameraData(unsigned short rawHeightData[], int heightOut[], int size, int distanceHighPoint, int distanceLowPoint) {
		int current = 0;
		for (int index = 0; index < size; index++) {
			current = rawHeightData[index];
			//Clipping
			if (current < distanceHighPoint) {
			current = distanceHighPoint;
			} if (current > distanceLowPoint) {
			current = distanceLowPoint;
			}
			
			current = (int)(1000 + (-1000) * ((float)(current - distanceHighPoint) / (float)(distanceLowPoint - distanceHighPoint)));
			heightOut[index] = current;
		}
	
	}
}


