[TOC]



## Baustellen Ende Mai

- [x] Pipeline
  - [x] Adapterklasse für die RealSense
    - [ ] Ausgelagert //nächste Woche
  - [ ] Sperr Map
  - [x] Bilineare Interpolation
    - [ ] C++			
    - [x] C# 
    - [ ] GUI 
  - [ ] Filter
    - [ ] Lowpass
    - [ ] HighPass
    - [ ] Rausrechnen der Steigung
  - [x] Mesh Generierung
    - [ ] Aufteilung in Sub-Meshes
  - [ ] New Map Generierung 
  - [x] Shader
    - [x] Höhenmap
    - [ ] NormalenMap mit Felsenstrukutur
  - [x] Quad verzerren für die Projektion
- [ ] UML Klassen Diagramm *nächste Woche*
- [ ] UML Sequenz Diagramm *nächste Woche*



 

## Beispielkonfiguration

Eine Beispielkonfiguration könnte wiefolgt aussehen: 

**Gameobjekts in der Scene:**

==RsDevice== (Prefab aus den LibRealsense Assets)

==Adapter== (das RSAdapter Skript aus dem LibSandBox Ordner als Component hinzufügen)

==SandboxPipeline== (das SandboxPipeline Script aus dem LibSandBox Ordner als Component hinzufügen)

==Quad==[^Quad]



[^Quad]: possible not required in the future 


 **RsDevice:**

- Live

- Profiles
  - Size: 1
    - Stream: Depth
    - Format: Z16
    - Framerate:30
    - Width: 1280
    - Height: 720

SandBoxPipeline:



## Benutzung

### Object Type: 

Der Objekttype beschreibt, wie die Höhenkarte gerendert werden soll. 

Verfügbar sind in der aktuellen Version: 

- Mesh
- Texture on Quad for Projection



#### Width/Height:

Die Daten der RealSense haben in der Regel eine Dimension von 1280x720, jedoch ist es aus Performancetechnischen Gründen unter Umständen nicht sinnvoll, mit der vollen Auflösung zu arbeiten. Um das RealSense Signal abzutasten, kann eine custom width und height von dem Benutzer eingestellt werden. 

Die Width und Height können auch von dem Standart Seitenverhältniss von 16:9 abweichen. 

Die Eingegebene Auflösung muss mit den Auflösungsstufen der Lockmap kompatibel sein, falls diese Aktiviert ist. 

Dazu muss sowohl die Width, als auch die Height ein Vielfaches von der eingegebenen Rastergröße sein. 

Beispiel ist die Konfiguration: 640x360 mit einer Rastergröße von 40 miteinander Kompatibel. Die Konfiguration 640x350 jedoch nicht. 

#### Auswahlbereich festlegen

Es kann ein Auswahlbereich von den Rohdaten der RealSense festgelegt werden, sodass nur der entsprechende ausgewählte Ausschnitt genutzt wird. 

Hierzu muss zur Laufzeit die Taste SPACE gedrückt werden. 

Danach kann jeder Ecke mit 7, 9, 1, 3 ausgewäht werden. Die aktuelle Mausposition wird hierbei als neuen Punkt gespeichert. Alternativ können die Punkte auch manuell in den Editor eingestellt werden. 

## Funktionsbeschreibung

### Textur erstellung

| C# Inline | C# Outline | C++    | Threaded |
| --------- | ---------- | ------ | -------- |
| 150.000   | 197.000    | 17.500 | -        |
| 142.000   | 201.000    | 14.700 | -        |
| Ø146      | Ø199       | Ø16.1  | -        |
| 9x        | 12x        | 1x     | -        |

**Dauer der Methode in verschiedenen Implementationen (in CPU Ticks per Method)**

### Bilineare Interpolation

#### Einsatz 

#### Funkionsweise

#### Performance

| C++                           | C#                              | C# Lookuptable |
| ----------------------------- | ------------------------------- | -------------- |
| 28.700 (ticks per Methodcall) | 627.000 (ticks per Method call) | -              |
| 1x                            | 21,8x                           | -              |

**Dauer der Methode in verschiedenen Implementationen (in CPU Ticks per Method)**

### Lockmap



TODO
- [ ] Weichzeichner Filter/ Sapial. (prozent von aktuellen übernehmen)
- [ ] Bresenham
- [ ] Lockmap Performance
- [ ] LookupTable Bilinear Interpolation



