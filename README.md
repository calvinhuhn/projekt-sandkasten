Unity Version 
The Sandbox Pipeline is only running under Unity Version 2018.3.11f1.

# Project Sandbox
## Beispielkonfiguration

Eine Beispielkonfiguration könnte wiefolgt aussehen: 

**Gameobjekts in der Scene:**

<mark>RsDevice</mark> (Prefab aus den LibRealsense Assets)

<mark>Adapter</mark> (das RSAdapter Skript aus dem LibSandBox Ordner als Component hinzufügen)

<mark>SandboxPipeline</mark> (das SandboxPipeline Script aus dem LibSandBox Ordner als Component hinzufügen)

<mark>Quad</mark>[^Quad]



[^Quad]: possible not required in the future 

 **RsDevice:**

- Live
- Profiles
  - Size: 1
    - Stream: Depth
    - Format: Z16
    - Framerate:30
    - Width: 1280
    - Height: 720

SandBoxPipeline:



Sandbox Pipeline:

Points:

Upper Left: 250, 500

Upper Right: 933, 598

Lower Right: 1025, 92

Lower Left: 284, 26

## Benutzung

### Object Type: 

Der Objekttype beschreibt, wie die Höhenkarte gerendert werden soll. 

Verfügbar sind in der aktuellen Version: 

- Mesh
- Texture on Quad for Projection



#### Width/Height:

Die Daten der RealSense haben in der Regel eine Dimension von 1280x720, jedoch ist es aus Performancetechnischen Gründen unter Umständen nicht sinnvoll, mit der vollen Auflösung zu arbeiten. Um das RealSense Signal abzutasten, kann eine custom width und height von dem Benutzer eingestellt werden. 

Die Width und Height können auch von dem Standart Seitenverhältniss von 16:9 abweichen. 

Die Eingegebene Auflösung muss mit den Auflösungsstufen der Lockmap kompatibel sein, falls diese Aktiviert ist. 

Dazu muss sowohl die Width, als auch die Height ein Vielfaches von der eingegebenen Rastergröße sein. 

Beispiel ist die Konfiguration: 640x360 mit einer Rastergröße von 40 miteinander Kompatibel. Die Konfiguration 640x350 jedoch nicht. 

#### Auswahlbereich festlegen

Es kann ein Auswahlbereich von den Rohdaten der RealSense festgelegt werden, sodass nur der entsprechende ausgewählte Ausschnitt genutzt wird. 

Hierzu muss zur Laufzeit die Taste SPACE gedrückt werden. 

Danach kann jeder Ecke mit 7, 9, 1, 3 ausgewäht werden. Die aktuelle Mausposition wird hierbei als neuen Punkt gespeichert. Alternativ können die Punkte auch manuell in den Editor eingestellt werden. 

## Einleitung und Aufgabenbereich

Mein Aufgabenbereich bestand zu einem Großteil aus der Entwicklung einer Pipeline, die intern Höhendaten einer Intel RealSense Tiefenkamera verarbeitet und die anschließend in Teilstücke (sogenannte Chunks) aufteilt. In weiterer Teilbereich ist die Modellierung und Animation von 3D Modellen sowie die Implementierung in Unity gewesen. 

## Requirements

This project requires a depth sensor (e.g Intel Realsense), a projector, a sandbox and Unity.
The Sandbox Pipeline is only running under Unity Version 2018.3.11f1.

---

## Changelog 

All notable changes to this project will be documented in this file.  
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).  

### [Unreleased]
### Added

**Framework-Class**
- Source Selection
- Performance Tests added


## [0.0.1] - 2019-03-29
### Added
